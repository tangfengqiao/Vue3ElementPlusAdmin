import { createApp } from 'vue'
import App from './App.vue'
import router from "./route";
import { createPinia } from "pinia";
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { useGlobalStore } from "./store/index.js";
//import 'element-plus/theme-chalk/dark/css-vars.css'

const app = createApp(App);
app.use(createPinia());
app.use(ElementPlus)
app.use(router);

const store = useGlobalStore()
router.beforeEach((to) => {
    store.setMenuPath(to.fullPath)
})

app.mount("#app");