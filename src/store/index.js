import { defineStore } from 'pinia'

export const useGlobalStore = defineStore('global', {
    state: () => ({
        username: '-',
        menuPath: '/'
    }),
    actions: {
        setUsername(username) {
            this.username = username
        },
        setMenuPath(path) {
            this.menuPath = path
        }
    },
})

