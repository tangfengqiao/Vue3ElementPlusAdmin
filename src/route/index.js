import { createRouter, createWebHistory } from "vue-router";
import { useMenu } from "@/api"

var arr = []

useMenu((data) => {
  data.forEach((item, index) => {
    arr.push({ path: item.path, name: item.path, component: import("@/views/" + item.component) })
    if (item.children) {
      arr[index].children = []
      item.children.forEach((j) => {
        arr.push({ path: j.path, name: j.path, component: import("@/views/" + j.component) })
      })
    }
  })
})



console.log(arr)

const routes = [
  {
    path: "/",
    name: "home",
    component: import("@/views/layout.vue"),
    redirect: '/dashboard',
    children: arr
  },
  {
    path: "/login",
    name: "login",
    component: import("../views/login.vue"),
  },
  {
    path: "/error",
    name: "error",
    component: import("../views/error.vue"),
  },
  {
    path: "/:pathMatch(.*)*",
    name: "default",
    redirect: "/error",
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});
export default router;
